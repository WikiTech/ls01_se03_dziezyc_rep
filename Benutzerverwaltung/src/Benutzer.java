public class Benutzer {
	private int benutzernummer;
	private String name;
	
	public Benutzer(String name, int benutzernummer) {
		this.name = name;
		this.benutzernummer = benutzernummer;
	}
	
	public int getBenutzernummer() {
		return this.benutzernummer;
	}
	
	public void setBenutzernummer(int benutzernummer) {
		this.benutzernummer = benutzernummer;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() { //(Name: Max, Benutzernummer: 1000))
		return "(Name: " + this.name + " , Benutzernummer: " + this.benutzernummer + ")";
		
	}
}
