﻿import java.util.Scanner;
import java.text.*;
import java.math.BigDecimal;
import java.math.RoundingMode;

class Fahrkartenautomat
{
    public static double fahrkartenbestellungErfassen()
    {
    	Scanner tastatur = new Scanner(System.in);
        System.out.print("Zu zahlender Betrag (EURO): ");
        double Betrag = tastatur.nextDouble();
        System.out.print("Anzahl der Tickets: ");
        int anzahlFahrscheine = tastatur.nextInt();
        return anzahlFahrscheine * Betrag;
        
        
    }	
    public static double fahrkartenBezahlen(double betrag)
    {
    	Scanner tastatur = new Scanner(System.in);
        double eingezahlterGesamtbetrag = 0.0;
        DecimalFormat f = new DecimalFormat("#0.00");
        while(eingezahlterGesamtbetrag < betrag)
        {
        	double verbleibend = betrag - eingezahlterGesamtbetrag;
     	   System.out.println("Noch zu zahlen: " + (f.format(verbleibend)) + " Euro");
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   double eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
            
        }
        return eingezahlterGesamtbetrag;
    }
	public static void fahrkartenAusgeben()
	{
		System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");	
	}
	public static void rueckgeldAusgeben(double eingezahlt, double betrag)
	{
		DecimalFormat f = new DecimalFormat("#0.00");
		double rückgabebetrag = eingezahlt - betrag;
		rückgabebetrag = runden(rückgabebetrag);
	       if(rückgabebetrag > 0.0)
	       {
	    	   System.out.println("Der Rückgabebetrag in Höhe von " + f.format(rückgabebetrag) + " EURO");
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag = runden(rückgabebetrag - 2);
	           }
	           //System.out.println(rückgabebetrag);
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
	        	  rückgabebetrag = runden(rückgabebetrag - 1);
	           }
	           //System.out.println(rückgabebetrag);
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
	        	  rückgabebetrag = runden(rückgabebetrag - 0.5);
	           }
	           //System.out.println(rückgabebetrag);
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	        	  rückgabebetrag = runden(rückgabebetrag - 0.2);
	           }
	           //System.out.println(rückgabebetrag);
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
	        	  rückgabebetrag = runden(rückgabebetrag - 0.1);
	           }
	           //System.out.println(rückgabebetrag);
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	        	  rückgabebetrag = runden(rückgabebetrag - 0.05);
	           }
	       }

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.");
	}
	
	public static double runden(double zahl)
	{
	      BigDecimal bd = new BigDecimal(zahl).setScale(2, RoundingMode.HALF_UP);
	      return bd.doubleValue();
	}
	
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;
       double verbleibend;
       int anzahlFahrscheine;
       


       
       zuZahlenderBetrag = fahrkartenbestellungErfassen();


       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       // Fahrscheinausgabe
       // -----------------
       fahrkartenAusgeben();

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rueckgeldAusgeben(eingezahlterGesamtbetrag,zuZahlenderBetrag);
    }
}