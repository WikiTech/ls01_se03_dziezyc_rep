import java.text.DecimalFormat;
import java.util.Scanner;

public class temperaturUmrechnung {
	public static void main (String[] args) {
		
		DecimalFormat f = new DecimalFormat("#0.0");
		Scanner myScanner = new Scanner(System.in);
		double startwertCelsius;
		double endwertCelsius;
		int schrittwert;
		double fahrenheit;
		System.out.print("Bitte den Startwert in Celsius eingeben: ");
		startwertCelsius = myScanner.nextDouble();
		System.out.print("Bitte den Endwert in Celsius eingeben: ");
		endwertCelsius = myScanner.nextDouble();
		System.out.print("Bitte die Schrittweite in Grad Celsius eingeben: ");
		schrittwert = myScanner.nextInt();
		
		while (startwertCelsius != endwertCelsius) {
			fahrenheit = (startwertCelsius * 1.8) + 32;
			System.out.println(startwertCelsius + "*C " + f.format(fahrenheit) + "*F");
			startwertCelsius = startwertCelsius + schrittwert;

		}  
		
	}
}