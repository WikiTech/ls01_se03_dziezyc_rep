﻿import java.util.Scanner;

public class Variablen {
  public static void main(String [] args){
	  Scanner myScanner = new Scanner(System.in);
    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
          Vereinbaren Sie eine geeignete Variable */
short programmLaeufe;
    /* 2. Weisen Sie dem Zaehler den Wert 25 zu
          und geben Sie ihn auf dem Bildschirm aus.*/
programmLaeufe = 25;
System.out.println(programmLaeufe);
    /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
          eines Programms ausgewaehlt werden.
          Vereinbaren Sie eine geeignete Variable */
System.out.println("Please press: a");
String menu;
menu= myScanner.next();


    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
          und geben Sie ihn auf dem Bildschirm aus.*/
String a = "C";
if (menu.equals("a")) {
	System.out.println(a);
}
    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
          notwendig.
          Vereinbaren Sie eine geeignete Variable */
double astronomischeWerte;
    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
          und geben Sie sie auf dem Bildschirm aus.*/
astronomischeWerte= 3E5;
    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
          soll die Anzahl der Mitglieder erfasst werden.
          Vereinbaren Sie eine geeignete Variable und initialisieren sie
          diese sinnvoll.*/
short zaehlerVerein = 7;
    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
System.out.println(zaehlerVerein);
    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
          dem Bildschirm aus.*/
double ladung = 1.60217646E-19f;
System.out.println(ladung);
    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
          Vereinbaren Sie eine geeignete Variable. */
boolean erfolg;
    /*11. Die Zahlung ist erfolgt.
          Weisen Sie der Variable den entsprechenden Wert zu
          und geben Sie die Variable auf dem Bildschirm aus.*/
erfolg = true;
System.out.println(erfolg);
  }//main
}// Variablen