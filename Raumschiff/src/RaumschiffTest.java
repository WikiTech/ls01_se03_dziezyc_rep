public class RaumschiffTest {
	public static void main (String[] args) {
	
	Raumschiff klingonen = new Raumschiff();
	Raumschiff romulaner = new Raumschiff();
	Raumschiff vulkanier = new Raumschiff();
	
	klingonen.setPhotonentorpedoAnzahl(1);
	klingonen.setEnergieversorgungInProzent(100);
	klingonen.setSchildeInProzent(100);
	klingonen.setHuelleInProzent(100);
	klingonen.setLebenserhaltungssystemeInProzent(100);
	klingonen.setAndroidenAnzahl(2);
	klingonen.setSchiffsname("IKS Hengh'ta");
	
	System.out.println(klingonen.getPhotonentorpedoAnzahl());
	System.out.println(klingonen.getEnergieversorgungInProzent());
	System.out.println(klingonen.getSchildeInProzent());
	System.out.println(klingonen.getHuelleInProzent());
	System.out.println(klingonen.getLebenserhaltungssystemeInProzent());
	System.out.println(klingonen.getAndroidenAnzahl());
	System.out.println(klingonen.getSchiffsname());
	System.out.println("");
	
	romulaner.setPhotonentorpedoAnzahl(2);
	romulaner.setEnergieversorgungInProzent(100);
	romulaner.setSchildeInProzent(100);
	romulaner.setHuelleInProzent(100);
	romulaner.setLebenserhaltungssystemeInProzent(100);
	romulaner.setAndroidenAnzahl(2);
	romulaner.setSchiffsname("IRW Khazara");
	
	System.out.println(romulaner.getPhotonentorpedoAnzahl());
	System.out.println(romulaner.getEnergieversorgungInProzent());
	System.out.println(romulaner.getSchildeInProzent());
	System.out.println(romulaner.getHuelleInProzent());
	System.out.println(romulaner.getLebenserhaltungssystemeInProzent());
	System.out.println(romulaner.getAndroidenAnzahl());
	System.out.println(romulaner.getSchiffsname());
	System.out.println("");
	
	vulkanier.setPhotonentorpedoAnzahl(0);
	vulkanier.setEnergieversorgungInProzent(80);
	vulkanier.setSchildeInProzent(80);
	vulkanier.setHuelleInProzent(50);
	vulkanier.setLebenserhaltungssystemeInProzent(100);
	vulkanier.setAndroidenAnzahl(5);
	vulkanier.setSchiffsname("Ni'Var");
	
	System.out.println(vulkanier.getPhotonentorpedoAnzahl());
	System.out.println(vulkanier.getEnergieversorgungInProzent());
	System.out.println(vulkanier.getSchildeInProzent());
	System.out.println(vulkanier.getHuelleInProzent());
	System.out.println(vulkanier.getLebenserhaltungssystemeInProzent());
	System.out.println(vulkanier.getAndroidenAnzahl());
	System.out.println(vulkanier.getSchiffsname());
	System.out.println("");
	
	
	}
}
